<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		Welcome ${user.firstName} ${user.lastName}
	</a> <a class="btn btn-outline-dark my-2 my-sm-0" href="logout"
		aria-disabled="true">Logout</a> </nav>

	<ul class="nav nav-pills">
		<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
			data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
			aria-expanded="false">Account</a>
			<div class="dropdown-menu">
				<c:if test="${account == null}">
					<a class="dropdown-item" href="createsavingsaccount">Create
						Savings Account</a>
				</c:if>
				<c:if test="${account != null}">
					<a class="dropdown-item" data-toggle="modal"
						data-target="#termModal">Create Term Account</a>
				</c:if>
			</div></li>

		<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
			data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
			aria-expanded="false">Funds Transfer</a>
			<div class="dropdown-menu">
				<a class="dropdown-item" href="loadbeneficiary">Add Beneficiary</a> <a
					class="dropdown-item" href="loadtransfer">Fund Transfer</a>
			</div></li>
			<li class="nav-item"><a class="nav-link" href="viewtransactions">Transactions</a></li>
	</ul>

	<div class="card">
		<div class="card-header">Account Summary</div>
		<div class="card-body">
			<c:if test="${account == null}">
				<div class="alert alert-dark" role="alert">Kindly create your
					Savings Account.</div>
			</c:if>
			<c:if test="${account != null}">
				<h5 class="card-title">Savings Account Details</h5>
				<table class="table">
					<thead>
						<tr>
							<th scope="col">Account Number</th>
							<th scope="col">Account Type</th>
							<th scope="col">Account Balance</th>
							<th scope="col">Created Date</th>
							<th scope="col">Updated Date</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${account.accountId}</td>
							<td>SAVINGS</td>
							<td>${account.accountBalance}</td>
							<td>${account.accountCreatedDate}</td>
							<td>${account.accountUpdatedDate}</td>
							<td><button type="button" class="btn btn-primary">View</button></td>
						</tr>
					</tbody>
				</table>
			</c:if>
			<c:if test="${taccounts != null}">
				<h5 class="card-title">Term Account Details</h5>
				<c:forEach items="${taccounts}" var="tacc">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Account Number</th>
								<th scope="col">Account Type</th>
								<th scope="col">Term Amount</th>
								<th scope="col">Term Tenure</th>
								<th scope="col">Interest</th>
								<th scope="col">Created Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${tacc.accountId}</td>
								<td>TERM</td>
								<td>${tacc.accountBalance}</td>
								<td>${tacc.depositTenure}</td>
								<td>${tacc.interestRate}</td>
								<td>${tacc.accountCreatedDate}</td>
							</tr>
						</tbody>
					</table>
				</c:forEach>
			</c:if>
		</div>
	</div>
	<c:if test="${error != null}">
		<div class="alert alert-danger" role="alert">
			<c:out value="${error}" />
		</div>
	</c:if>
	<!-- Modal -->
	<div class="modal fade" id="termModal" tabindex="-1" role="dialog"
		aria-labelledby="termModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="termModalLabel">Term Account
						Creation</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form:form action="createtermaccount" method="post"
						modelAttribute="taccount">
						<div class="form-row">
							<div class="form-group col-md-6">
								<form:label path="accountBalance" for="accountBalance">Term Amount</form:label>
								<form:input path="accountBalance" type="text"
									class="form-control" placeholder="" />
							</div>
							<div class="form-group col-md-6">
								<form:label path="depositTenure" for="depositTenure">Tenure</form:label>
								<form:select class="custom-select" path="depositTenure">
									<option selected value="1">One Year</option>
									<option value="2">Two Years</option>
									<option value="3">Three Years</option>
									<option value="4">Four Years</option>
									<option value="5">Five Years</option>
								</form:select>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-9"></div>
							<!-- <div class="form-group col-md-2">
								<button type="button" class="btn btn-warning">Reset</button>
							</div> -->
							<div class="form-group col-md-3">
								<button type="submit" class="btn btn-primary">Register</button>
							</div>
							<!-- <div class="form-group col-md-4"></div> -->
						</div>

					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>