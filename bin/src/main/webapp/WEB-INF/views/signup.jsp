<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		DhanJyoti - Registration
	</a> </nav>
	<div class="container">
		<form:form action="save" method="post" modelAttribute="user">
			<div class="form-row">
				<div class="form-group col-md-4">
					<form:label path="firstName" for="firstName">First Name</form:label>
					<form:input path="firstName" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="lastName" for="lastName">Last Name</form:label>
					<form:input path="lastName" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="textDob" for="textDob">DOB</form:label>
					<form:input path="textDob" type="text" class="form-control"
						placeholder="mm/dd/yyyy" />
				</div>
			</div>

			<div class="form-row">
				<div class="form-group col-md-4">
					<form:label path="addLine1" for="addLine1">Address Line 1</form:label>
					<form:input path="addLine1" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="addLine2" for="addLine2">Address Line 2</form:label>
					<form:input path="addLine2" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="city" for="city">City</form:label>
					<form:input path="city" type="text" class="form-control"
						placeholder="" />
				</div>
			</div>

			<div class="form-row">
				<div class="form-group col-md-4">
					<form:label path="state" for="state">State</form:label>
					<form:input path="state" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="pin" for="pin">Pin Code</form:label>
					<form:input path="pin" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="mobileNumber" for="mobileNumber">Mobile Number</form:label>
					<form:input path="mobileNumber" type="text" class="form-control"
						placeholder="" />
				</div>
			</div>

			<div class="form-row">
				<div class="form-group col-md-4">
					<form:label path="emailId" for="emailId">Email</form:label>
					<form:input path="emailId" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="aadharId" for="aadharId">Aadhar</form:label>
					<form:input path="aadharId" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="pan" for="pan">Pan</form:label>
					<form:input path="pan" type="text" class="form-control"
						placeholder="" />
				</div>
			</div>

			<div class="form-row">
				<div class="form-group col-md-4">
					<form:label path="userName" for="userName">Username</form:label>
					<form:input path="userName" type="text" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="password" for="password">Password</form:label>
					<form:input path="password" type="password" class="form-control"
						placeholder="" />
				</div>
				<div class="form-group col-md-4">
					<form:label path="confirmPassword" for="confirmPassword">Confirm Password</form:label>
					<form:input path="confirmPassword" type="password"
						class="form-control" placeholder="" />
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Register</button>
		</form:form>
		<c:if test="${error != null}">
			<div class="alert alert-danger" role="alert">
				<c:out value="${error}" />
			</div>
		</c:if>
	</div>
</body>
</html>