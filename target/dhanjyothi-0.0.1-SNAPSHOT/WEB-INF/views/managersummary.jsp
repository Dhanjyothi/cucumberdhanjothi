<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		Welcome ${user.firstName} ${user.lastName}
	</a> <a class="btn btn-outline-dark my-2 my-sm-0" href="logout"
		aria-disabled="true">Logout</a> </nav>

	<div class="card">
		<div class="card-header">Manage Bank Users</div>
		<div class="card-body">
			<c:if test="${users == null}">
				<div class="alert alert-dark" role="alert">No Users Found</div>
			</c:if>
			<c:if test="${users != null}">
				<h5 class="card-title">User Details</h5>
				<table class="table">
					<thead>
						<tr>
							<th scope="col">User Name</th>
							<th scope="col">DOB</th>
							<th scope="col">Address</th>
							<th scope="col">Mobile</th>
							<th scope="col">Email</th>
							<th scope="col">Aadhar</th>
							<th scope="col">PAN</th>
							<th scope="col">Status</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${users}" var="user">
							<c:if test="${user.userRole eq 'C'.charAt(0)}">
								<tr>
									<td>${user.firstName} ${user.lastName}</td>
									<td><a href="download/${user.userName}/dob">${user.dob}</a></td>
									<td><a href="download/${user.userName}/address">${user.addLine1} ${user.addLine2}, ${user.city}
										${user.state} (${user.pin})</a></td>
									<td>${user.mobileNumber}</td>
									<td>${user.emailId}</td>
									<td><a href="download/${user.userName}/aadhar">${user.aadharId}</a></td>
									<td>${user.pan}</td>
									<td>${user.userStatus == 'A'.charAt(0) ? "Active" : "New"}</td>
									<c:if test="${user.userStatus eq 'N'.charAt(0)}">
										<td><a class="btn btn-primary btn-sm" href="accsummary?approve=${user.userId}">Approve</a></td>
										<td><a class="btn btn-danger btn-sm" href="accsummary?reject=${user.userId}">Reject</a></td>
									</c:if>
									<c:if test="${user.userStatus eq 'A'.charAt(0)}">
										<td></td>
									</c:if>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</c:if>
		</div>
	</div>
	<c:if test="${error != null}">
		<div class="alert alert-danger" role="alert">
			<c:out value="${error}" />
		</div>
	</c:if>
</body>
</html>