<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">

<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		Welcome ${user.firstName} ${user.lastName}
	</a> <a class="btn btn-outline-dark my-2 my-sm-0" href="logout"
		aria-disabled="true">Logout</a> </nav>

	<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="accsummary">Account
				Summary</a></li>
		<li class="breadcrumb-item active" aria-current="page">Transactions</li>
	</ol>
	</nav>

	<div class="card">
		<div class="card-header">Transactions</div>
		<div class="card-body">
			<c:if test="${transactions == null || accountNumber == null}">
				<div class="alert alert-dark" role="alert">No transactions
					available.</div>
			</c:if>
			<c:if test="${transactions != null && accountNumber != null}">

				<table class="table" id="myTable">
					<thead>
						<tr>
							<th scope="col">Transaction Id</th>
							<th scope="col">Transaction Description</th>
							<th scope="col">Transaction Amount</th>
							<th scope="col">Transaction Type</th>
							<th scope="col">Transaction Date</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${transactions}" var="transaction">
							<tr>
								<td>${transaction.trnId}</td>
								<td>${transaction.trnDesc}</td>
								<td>${transaction.trnAmount}</td>
								<td>${transaction.creditAccount == accountNumber ? "CREDIT" : "DEBIT"}</td>
								<td>${transaction.trnDateTime}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				 <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
  <script>
  $(function(){
    $("#myTable").dataTable();
  })
  </script>
			</c:if>
		</div>
	</div>
</body>
</html>