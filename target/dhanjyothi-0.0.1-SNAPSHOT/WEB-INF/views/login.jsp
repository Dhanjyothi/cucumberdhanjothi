<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		DhanJyoti - Login
	</a> </nav>
	<div class="container">
		<div class="jumbotron">
			<h2>Login</h2>
			<form:form action="submit" method="post" modelAttribute="user">
				<div class="form-group">
					<form:label path="userName" for="userName">Username</form:label>
					<form:input type="text" class="form-control" id="userName"
						path="userName" />
				</div>
				<div class="form-group">
					<form:label path="password" for="password">Password</form:label>
					<form:input type="password" class="form-control" id="password"
						path="password" />
				</div>
				<button type="submit" class="btn btn-primary">Login</button>
				<div class="alert" role="alert">
					Don't have an account? <a href="signup" class="alert-link">Sign
						Up</a>
				</div>
			</form:form>
			<c:if test="${error != null}">
				<div class="alert alert-danger" role="alert">
					<c:out value="${error}" />
				</div>
			</c:if>
			<c:if test="${success != null}">
				<div class="alert alert-success" role="alert">
					<c:out value="${success}" />
				</div>
			</c:if>
		</div>
	</div>
</body>
</html>