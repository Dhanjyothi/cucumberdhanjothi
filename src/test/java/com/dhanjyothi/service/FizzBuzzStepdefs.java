package com.dhanjyothi.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.dhanjyothi.model.User;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class FizzBuzzStepdefs {

    LoginService loginservice;
    RegisterService registerservice; 
   
    User user;
    List<User> userList;

       @Given("^I navigate to Dhanjyothi$")
    public void i_navigate_to_Dhanjyothi() throws Throwable {
        user =new User();
        userList=new ArrayList<User>();
    }

    @When("^I enter the firstname as \"([^\"]*)\" and lastname as \"([^\"]*)\"$")
    public void i_enter_the_firstname_as_and_lastname_as(String fname, String lname) throws Throwable {
        user.setFirstName(fname);
        user.setLastName(lname);
        userList.add(user);
    }
    @Then("^I clicked register button$")
    public void i_clicked_register_button() throws Throwable {
        for (User userObj : userList) {
           System.out.println( userObj.getFirstName());
                       Assert.assertSame(user, userObj);
               
        }
    	
    }
}
