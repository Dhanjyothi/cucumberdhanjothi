<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		Welcome ${user.firstName} ${user.lastName}
	</a> <a class="btn btn-outline-dark my-2 my-sm-0" href="logout"
		aria-disabled="true">Logout</a> </nav>

	<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="accsummary">Account
				Summary</a></li>
		<li class="breadcrumb-item active" aria-current="page">Beneficiary
			Type</li>
	</ol>
	</nav>

	<div class="card">
		<div class="card-header">Select Beneficiary Type</div>
		<div class="card-body bg-light">
			<div class="container">
				<div class="row">
					<div class="col-sm">
						<a class="btn btn-info" href="beneficiarywithinbank" role="button">Transfer Within Bank</a>
					</div>
					<div class="col-sm"></div>
					<div class="col-sm">
						<a class="btn btn-info" href="beneficiaryotherbank" role="button">Transfer To Other Bank</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>