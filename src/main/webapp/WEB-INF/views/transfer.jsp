<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		Welcome ${user.firstName} ${user.lastName}
	</a> <a class="btn btn-outline-dark my-2 my-sm-0" href="logout"
		aria-disabled="true">Logout</a> </nav>

	<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="accsummary">Account
				Summary</a></li>
		<li class="breadcrumb-item active" aria-current="page">Fund
			Transfer</li>
	</ol>
	</nav>

	<div class="card">
		<div class="card-header">Fund Transfer</div>
		<div class="card-body">
			<form:form action="transferamt" method="post"
				modelAttribute="transaction">
				<div class="form-row">
					<div class="form-group col-md-4">
						<form:label path="creditAccount" for="creditAccount">Beneficiaries</form:label>
						<form:select class="custom-select" path="creditAccount">
							<c:forEach items="${sessionScope.beneficiaries}" var="beneficiary">
								<option value="${beneficiary.benAccountNum}">${beneficiary.benNickName}</option>
							</c:forEach>
						</form:select>
					</div>
					<div class="form-group col-md-4">
						<form:label path="trnAmount" for="trnAmount">Amount</form:label>
						<form:input path="trnAmount" type="text" class="form-control"
							placeholder="" />
					</div>
					<div class="form-group col-md-4">
						<form:label path="trnDesc" for="trnDesc">Remarks</form:label>
						<form:input path="trnDesc" type="text" class="form-control"
							placeholder="" />
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-5"></div>
					<div class="form-group col-md-4">
						<button type="submit" class="btn btn-primary">Transfer</button>
					</div>
					<div class="form-group col-md-3"></div>
				</div>
				<c:if test="${error != null}">
					<div class="alert alert-danger" role="alert">
						<c:out value="${error}" />
					</div>
				</c:if>
				<c:if test="${success != null}">
					<div class="alert alert-success" role="alert">
						<c:out value="${success}" />
					</div>
				</c:if>
			</form:form>
		</div>
	</div>
</body>
</html>