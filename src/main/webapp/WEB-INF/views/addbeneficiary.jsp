<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/bootstrap-4.4.1-dist/js/jquery-3.4.1.min.js"></script>
<script src="resources/bootstrap-4.4.1-dist/js/popper.min.js"></script>
<link href="resources/bootstrap-4.4.1-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="resources/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
<title>DhanJyoti Internet App</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light"> <a
		class="navbar-brand" href="#"> <img src="resources/logo.jpg"
		width="30" height="30" class="d-inline-block align-top" alt="">
		Welcome ${user.firstName} ${user.lastName}
	</a> <a class="btn btn-outline-dark my-2 my-sm-0" href="logout"
		aria-disabled="true">Logout</a> </nav>

	<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="accsummary">Account
				Summary</a></li>
		<li class="breadcrumb-item"><a href="loadbeneficiary">Beneficiary
				Type</a></li>
		<li class="breadcrumb-item active" aria-current="page">Add
			Beneficiary</li>
	</ol>
	</nav>

	<div class="card">
		<div class="card-header">Add Beneficiary</div>
		<div class="card-body">
			<form:form action="savebeneficiary" method="post"
				modelAttribute="beneficiary">
				<div class="form-row">
					<div class="form-group col-md-4">
						<form:label path="benNickName" for="benNickName">Beneficiary Nick Name</form:label>
						<form:input path="benNickName" type="text" class="form-control"
							placeholder="" />
					</div>
					<div class="form-group col-md-4">
						<form:label path="benName" for="benName">Beneficiary Name</form:label>
						<form:input path="benName" type="text" class="form-control"
							placeholder="" />
					</div>
					<div class="form-group col-md-4">
						<form:label path="benAccountNum" for="benAccountNum">Beneficiary Acc No.</form:label>
						<form:input path="benAccountNum" type="text" class="form-control"
							placeholder="" />
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-5"></div>
					<div class="form-group col-md-4">
						<button type="submit" class="btn btn-primary">Add
							Beneficiary</button>
					</div>
					<div class="form-group col-md-3"></div>
				</div>
				<c:if test="${error != null}">
					<div class="alert alert-danger" role="alert">
						<c:out value="${error}" />
					</div>
				</c:if>
			</form:form>
		</div>
	</div>
	<div class="card">
		<div class="card-header">Beneficiaries</div>
		<div class="card-body">
			<c:if test="${beneficiaries == null}">
				<div class="alert alert-dark" role="alert">No beneficiaries
					available.</div>
			</c:if>
			<c:if test="${beneficiaries != null}">
				<c:forEach items="${beneficiaries}" var="ben">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Beneficiary Name</th>
								<th scope="col">Beneficiary Nick Name</th>
								<th scope="col">Beneficiary Acc No</th>
								<th scope="col">Beneficiary Bank</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${ben.benName}</td>
								<td>${ben.benNickName}</td>
								<td>${ben.benAccountNum}</td>
								<td>${ben.benBank}</td>
							</tr>
						</tbody>
					</table>
				</c:forEach>
			</c:if>
		</div>
	</div>
</body>
</html>