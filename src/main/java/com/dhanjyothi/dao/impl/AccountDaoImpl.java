package com.dhanjyothi.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhanjyothi.dao.AccountDao;
import com.dhanjyothi.model.Account;
import com.dhanjyothi.model.Beneficiaries;
import com.dhanjyothi.model.Transaction;
import com.dhanjyothi.model.User;

@Repository
@Transactional
public class AccountDaoImpl implements AccountDao {
	@Autowired
	SessionFactory sessionFactory;

	public Account getAccountDetails(int userId, String accountType) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Account> criteria = builder.createQuery(Account.class);
		Root<Account> root = criteria.from(Account.class);
		criteria.select(root);

		criteria.where(builder.equal(root.get("accountHolderId"), userId), builder.equal(root.get("accountType"), 'S'));
		List<Account> accountList = session.createQuery(criteria).getResultList();
		if (accountList.isEmpty()) {
			return null;
		} else {
			return accountList.get(0);
		}
	}
	
	public Account getAccountDetails(int accountId) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Account.class, accountId);
	}

	public void openSavingsAccount(Account account, boolean isSavingsAccountExists) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(account);
	}

	public void openTermAccount(Account account) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
	}

	public List<Account> getTermAccountDetails(int userId, String accountType) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Account> criteria = builder.createQuery(Account.class);
		Root<Account> root = criteria.from(Account.class);
		criteria.select(root);

		criteria.where(builder.equal(root.get("accountHolderId"), userId), builder.equal(root.get("accountType"), 'T'));
		return session.createQuery(criteria).getResultList();
	}

	public User getUserDetails(User user) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);

		criteria.where(builder.equal(root.get("userName"), user.getUserName()));
		List<User> userList = session.createQuery(criteria).getResultList();
		if (userList.isEmpty()) {
			return null;
		} else {
			return userList.get(0);
		}
	}

	public void saveBeneficiaries(Beneficiaries beneficiaries) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(beneficiaries);
	}

	public Account checkAccountExists(int accountId) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Account> criteria = builder.createQuery(Account.class);
		Root<Account> root = criteria.from(Account.class);
		criteria.select(root);

		criteria.where(builder.equal(root.get("accountId"), accountId), builder.equal(root.get("accountType"), 'S'));
		List<Account> accountList = session.createQuery(criteria).getResultList();
		if (accountList.isEmpty()) {
			return null;
		} else {
			return accountList.get(0);
		}
	}

	public List<Beneficiaries> getAllBeneficiaries(int userId) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Beneficiaries> criteria = builder.createQuery(Beneficiaries.class);
		Root<Beneficiaries> root = criteria.from(Beneficiaries.class);
		criteria.select(root);

		criteria.where(builder.equal(root.get("ownerId"), userId));
		return session.createQuery(criteria).getResultList();
	}

	public void updateTransactionDetails(Transaction transaction) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(transaction);
	}

	public List<Transaction> loadAllTransactions(int accountId) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Transaction> criteria = builder.createQuery(Transaction.class);
		Root<Transaction> root = criteria.from(Transaction.class);
		criteria.select(root);

		criteria.where(builder.or(builder.equal(root.get("creditAccount"), accountId),
				builder.equal(root.get("debitAccount"), accountId)));
		criteria.orderBy(builder.desc(root.get("trnDateTime")));
		return session.createQuery(criteria).getResultList();
	}

	public User isUserNameExists(String userName) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);

		criteria.where(builder.equal(root.get("userName"), userName));
		List<User> userList = session.createQuery(criteria).getResultList();
		if (userList.isEmpty()) {
			return null;
		} else {
			return userList.get(0);
		}
	}

	public User getUserById(int userId) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		return session.get(User.class, userId);
	}

	@Override
	public void updateUser(User user) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(user);
	}

}
