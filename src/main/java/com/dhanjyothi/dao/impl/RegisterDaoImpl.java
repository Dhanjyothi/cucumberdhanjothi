package com.dhanjyothi.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhanjyothi.dao.RegisterDao;
import com.dhanjyothi.model.User;

@Repository
@Transactional
public class RegisterDaoImpl implements RegisterDao{
	@Autowired
	SessionFactory sessionFactory;
	
	public void saveRegister(User customer) {
		Session session = sessionFactory.getCurrentSession();
		session.save(customer);
	}

	public List<User> getAllUsers() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);

		List<User> userList = session.createQuery(criteria).getResultList();
		return userList;
	}


}
