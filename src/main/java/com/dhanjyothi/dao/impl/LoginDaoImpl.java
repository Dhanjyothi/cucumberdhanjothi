package com.dhanjyothi.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhanjyothi.dao.LoginDao;
import com.dhanjyothi.model.User;

@Repository
@Transactional
public class LoginDaoImpl implements LoginDao {

	@Autowired
	SessionFactory sessionFactory;

	public int validateUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);

		criteria.where(builder.equal(root.get("userName"), user.getUserName()),
				builder.equal(root.get("password"), user.getPassword()),
				builder.equal(root.get("userStatus"), 'A'));
		List<User> userList = session.createQuery(criteria).getResultList();
		if (userList.isEmpty()) {
			return 0;
		} else {
			return userList.get(0).getUserId();
		}
	}

	@Override
	public User getUser(Integer userId) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(User.class, userId);
	}
}