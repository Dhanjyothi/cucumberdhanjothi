package com.dhanjyothi.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.dhanjyothi.model.Beneficiaries;

@Component
public class DhanJyothiUtil {

	public Map<Integer, String> getTenureDetails() {
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		map.put(1, "One Year");
		map.put(2, "Two Year");
		map.put(3, "Three Year");
		map.put(4, "Four Year");
		map.put(5, "Five Year");
		return map;
	}

	public Date getCurrentDate() {
		Date date = java.util.Calendar.getInstance().getTime();
		return date;
	}

	public Date getTermMaturityDate(int year) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.YEAR, year);
		Date maturityDate = c.getTime();
		return maturityDate;
	}

	public float getInterstRate(int tenure, long amount) {
		float interest = 0.0f;
		if (tenure == 1) {
			if (amount <= 100000) {
				interest = 5.75f;
			} else if (amount <= 1000000) {
				interest = 6.00f;
			} else if (amount >= 1000000 && amount <= 10000000) {
				interest = 6.25f;
			} else {
				interest = 0.0f;
			}

		} else if (tenure == 2) {
			if (amount <= 100000) {
				interest = 5.75f;
			} else if (amount <= 1000000) {
				interest = 6.00f;
			} else if (amount >= 1000000 && amount <= 10000000) {
				interest = 6.50f;
			} else {
				interest = 0.0f;
			}

		} else if (tenure == 3) {
			if (amount <= 100000) {
				interest = 5.75f;
			} else if (amount <= 1000000) {
				interest = 6.25f;
			} else if (amount >= 1000000 && amount <= 10000000) {
				interest = 6.75f;
			} else {
				interest = 0.0f;
			}

		} else if (tenure == 4) {
			if (amount <= 100000) {
				interest = 6.00f;
			} else if (amount <= 1000000) {
				interest = 6.25f;
			} else if (amount >= 1000000 && amount <= 10000000) {
				interest = 6.75f;
			} else {
				interest = 0.0f;
			}

		} else if (tenure == 5) {
			if (amount <= 100000) {
				interest = 6.00f;
			} else if (amount <= 1000000) {
				interest = 6.50f;
			} else if (amount >= 1000000 && amount <= 10000000) {
				interest = 7.00f;
			} else {
				interest = 0.0f;
			}

		} else {
			interest = 0.0f;
		}
		
		return interest;
	}
	
	public String logout(HttpServletRequest request) {
		if (request != null) {
			request.getSession().invalidate();
		}
		return "logout";
	}
	
	public long getAccountBalance (long balance, long termAmount) {
		return 1L;
	}
	
	public long addAccountBalance(long balance, long termAmount) {
		return 1L;
	}
	
	public Beneficiaries getBeneficiary(long id,List<Beneficiaries> beneficiaries) {
		
		return null;
	}
	
	public String getAadharDirectory(String userName) {
		return "/dhanjyotifiles/" + userName + "/aadhar";
	}
	
	public String getDobDirectory(String userName) {
		return "/dhanjyotifiles/" + userName + "/dob";
	}
	
	public String getAddressDirectory(String userName) {
		return "/dhanjyotifiles/" + userName + "/address";
	}
	
	public boolean isValidDocFile(String filename) {
		return filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".jpeg")
				|| filename.toLowerCase().endsWith(".pdf");
	}
	
	public void saveDocFile(String dirpath, MultipartFile file) throws IOException {
		File dir = new File(dirpath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
		stream.write(file.getBytes());
		stream.close();
	}
	
	public Path getAadharFilePath(String userName) throws IOException {
		String dir = getAadharDirectory(userName);
		Optional<Path> path = Files.list(Paths.get(dir)).findFirst();
		if (path.isPresent()) {
			return path.get();
		}
		return null;
	}
	
	public Path getDobFilePath(String userName) throws IOException {
		String dir = getDobDirectory(userName);
		Optional<Path> path = Files.list(Paths.get(dir)).findFirst();
		if (path.isPresent()) {
			return path.get();
		}
		return null;
	}
	
	public Path getAddressFilePath(String userName) throws IOException {
		String dir = getAddressDirectory(userName);
		Optional<Path> path = Files.list(Paths.get(dir)).findFirst();
		if (path.isPresent()) {
			return path.get();
		}
		return null;
	}

}
