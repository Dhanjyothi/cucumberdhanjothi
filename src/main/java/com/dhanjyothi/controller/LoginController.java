package com.dhanjyothi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dhanjyothi.model.Account;
import com.dhanjyothi.model.User;
import com.dhanjyothi.service.AccountService;
import com.dhanjyothi.service.LoginService;
import com.dhanjyothi.service.RegisterService;
import com.dhanjyothi.util.DhanJyothiUtil;

@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@Autowired
	AccountService accountService;
	
	@Autowired
	RegisterService registrationService;

	@Autowired
	DhanJyothiUtil util;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showRegister(Model model) {
		model.addAttribute("user", new User());
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public String validateUser(@ModelAttribute("user") User user, HttpServletRequest request, Model model)
			throws Exception {
		String returnView = "login";
		Integer userId = loginService.validateUser(user);
		if (userId != 0) {
			request.getSession().setAttribute("user", loginService.getUser(userId));
			return loadAccSummary(request, model);
		} else {
			request.getSession().invalidate();
			returnView = "login";
			model.addAttribute("user", new User());
			model.addAttribute("error", "Invalid credentials or account not active.");
		}
		return returnView;
	}

	@RequestMapping("/accsummary")
	public String loadAccSummary(HttpServletRequest request, Model model) throws Exception {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		model.addAttribute("user", user);
		if (user.getUserRole() == 'M') {
			if (request.getParameter("approve") != null) {
				String approveUser = request.getParameter("approve");
				User approveUserObj = accountService.getUserById(Integer.parseInt(approveUser));
				approveUserObj.setUserStatus('A');
				accountService.updateUser(approveUserObj);
			} else if (request.getParameter("reject") != null) {
				//String rejectUser = request.getParameter("reject");
				//User rejectUserObj = accountService.getUserById(Integer.parseInt(rejectUser));
			}
			model.addAttribute("users", registrationService.getAllUsers());
			return "managersummary";
		} else {
			Account account = accountService.getAccountDetails(user.getUserId(), "S");
			request.getSession().setAttribute("account", account);
			model.addAttribute("account", account);
			model.addAttribute("taccount", new Account());
			List<Account> termAccounts = accountService.getTermAccountDetails(user.getUserId(), "T");
			request.getSession().setAttribute("taccounts", termAccounts);
			model.addAttribute("taccounts", request.getSession().getAttribute("taccounts"));
			return "accountsummary";
		}
	}

	@RequestMapping("/adduser")
	public String addUser(HttpServletRequest request, Model model) {
		return null;
	}

	@GetMapping("/logout")
	public String logoutPage(HttpServletRequest request) {
		return util.logout(request);
	}
}
