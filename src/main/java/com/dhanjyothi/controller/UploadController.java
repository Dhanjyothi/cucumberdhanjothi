package com.dhanjyothi.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.file.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dhanjyothi.model.UploadFile;
import com.dhanjyothi.model.User;
import com.dhanjyothi.util.DhanJyothiUtil;

@Controller
public class UploadController {
	
	@Autowired
	DhanJyothiUtil util;
	
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView showUpload() {

		return new ModelAndView("");

	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ModelAndView fileUploadPage(UploadFile uploadFile) throws IOException {

		return new ModelAndView("");

	}

	@RequestMapping(value = "/download/{username}/{filename}", method = RequestMethod.GET)
	public void fileDownloadPage(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("filename") String filename, @PathVariable("username") String username) throws IOException {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return;
		}
		String mimeType= null;
		Path path = null;
		if(filename.equalsIgnoreCase("aadhar")) {
			path = util.getAadharFilePath(username);
		} else if (filename.equalsIgnoreCase("dob")) {
			path = util.getDobFilePath(username);
		} else if (filename.equalsIgnoreCase("address")) {
			path = util.getAddressFilePath(username);
		}
		if (path != null) {
			File file = path.toFile();
			mimeType= URLConnection.guessContentTypeFromName(path.getFileName().toString());
			if(mimeType==null){
	            System.out.println("mimetype is not detectable, will take default");
	            mimeType = "application/octet-stream";
	        }
			System.out.println("mimetype : "+mimeType);
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + path.getFileName().toString() +"\""));
			response.setContentLength((int)file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		}
	}
}
