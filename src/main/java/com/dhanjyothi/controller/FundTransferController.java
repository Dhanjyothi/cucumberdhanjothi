package com.dhanjyothi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.dhanjyothi.model.Account;
import com.dhanjyothi.model.Beneficiaries;
import com.dhanjyothi.model.Transaction;
import com.dhanjyothi.model.User;
import com.dhanjyothi.service.AccountService;
import com.dhanjyothi.util.DhanJyothiUtil;

@Controller
public class FundTransferController {

	@Autowired
	AccountService accountService;

	@Autowired
	DhanJyothiUtil util;

	@GetMapping("/loadbeneficiary")
	public String loadBeneficiaryPage() {
		return "beneficiarytype";
	}

	@GetMapping("/loadtransfer")
	public String loadTransferPage(@ModelAttribute("transaction") Transaction transaction, Model model,
			HttpServletRequest request) throws Exception {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		List<Beneficiaries> beneficiaries = accountService.getAllBeneficiaries(user.getUserId());
		request.getSession().setAttribute("beneficiaries", beneficiaries);
		model.addAttribute("transaction", new Transaction());
		return "transfer";
	}

	@GetMapping("/beneficiarywithinbank")
	public String beneficiarywithinbank(@ModelAttribute("beneficiary") Beneficiaries beneficiary, Model model,
			HttpServletRequest request) throws Exception {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		model.addAttribute("beneficiary", new Beneficiaries());
		model.addAttribute("beneficiaries", accountService.getAllBeneficiaries(user.getUserId()));
		return "addbeneficiary";
	}

	@GetMapping("/beneficiaryotherbank")
	public String beneficiaryotherbank(@ModelAttribute("beneficiary") Beneficiaries beneficiary, Model model,
			HttpServletRequest request) throws Exception {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		model.addAttribute("beneficiary", new Beneficiaries());
		model.addAttribute("beneficiaries", accountService.getAllBeneficiaries(user.getUserId()));
		return "addbeneficiary";
	}

	@PostMapping("/savebeneficiary")
	public String saveBeneficiary(@ModelAttribute("beneficiary") Beneficiaries beneficiary, Model model,
			BindingResult bindingResult, HttpServletRequest request) throws Exception {
		String errorMsg = null;
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		if (beneficiary == null || beneficiary.getBenAccountNum() == null || beneficiary.getBenName() == null
				|| beneficiary.getBenNickName() == null) {
			errorMsg = "Please provide Beneficiary's Name,Nick Name and Account Number.";
		} else {
			Account account = accountService.getAccountDetails(user.getUserId(), "S");
			if (account == null) {
				errorMsg = "You cannot add Beneficiary unless you have a Savings Account.";
			} else if (beneficiary.getBenAccountNum().intValue() == account.getAccountId()) {
				errorMsg = "You cannot add yourself as Beneficiary.";
			} else if (!accountService.checkAccountExists(beneficiary)) {
				errorMsg = "The supplied Account Number does not exists.";
			}

			if (errorMsg == null) {
				List<Beneficiaries> beneficiaries = accountService.getAllBeneficiaries(user.getUserId());
				for (Beneficiaries ben : beneficiaries) {
					if (ben.getBenAccountNum() == beneficiary.getBenAccountNum()) {
						errorMsg = "Beneficiary with provided Account Number already exists.";
						break;
					}
				}
			}

			if (errorMsg == null) {
				beneficiary.setBenBank("Dhanjyoti");
				beneficiary.setBeneficiaryType('I');
				beneficiary.setBenIfsc("DHAN1234");
				beneficiary.setOwnerId(user.getUserId());
				accountService.saveBeneficiaries(account, beneficiary);
			}
		}

		if (errorMsg != null) {
			model.addAttribute("error", errorMsg);
		}
		model.addAttribute("beneficiary", new Beneficiaries());
		model.addAttribute("beneficiaries", accountService.getAllBeneficiaries(user.getUserId()));
		return "addbeneficiary";
	}

	@PostMapping("/transferamt")
	public String transferAmount(@ModelAttribute("transaction") Transaction transaction, Model model,
			BindingResult bindingResult, HttpServletRequest request) throws Exception {
		String errorMsg = null;
		String success = "Transaction Successful!";
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		Account sbAccount = accountService.getAccountDetails(user.getUserId(), "S");
		if (transaction == null || transaction.getCreditAccount() == null || transaction.getTrnAmount() == null
				|| transaction.getTrnDesc() == null || "".equals(transaction.getTrnDesc())) {
			errorMsg = "Please provide Beneficiary, Amount and Transaction Remark.";
		} else if(sbAccount == null) {
			errorMsg = "No don't have an Savings account to transfer money.";
		} else if(sbAccount.getAccountBalance().floatValue() < transaction.getTrnAmount().floatValue()) {
			errorMsg = "No don't have enough balance to transfer money.";
		} else {
			accountService.updateFromAccount(sbAccount, transaction.getTrnAmount().longValue(), transaction);
			accountService.updateToAccount(transaction);
			model.addAttribute("success", success);
			model.addAttribute("transaction", new Transaction());
		}
		model.addAttribute("error", errorMsg);
		return "transfer";
	}
}
