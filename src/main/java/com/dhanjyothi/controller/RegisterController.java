package com.dhanjyothi.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.dhanjyothi.model.User;
import com.dhanjyothi.service.AccountService;
import com.dhanjyothi.service.RegisterService;
import com.dhanjyothi.util.DhanJyothiUtil;

@Controller
public class RegisterController {

	@Autowired
	RegisterService registerService;

	@Autowired
	AccountService accountService;

	@Autowired
	DhanJyothiUtil util;

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public ModelAndView showRegister() {
		ModelAndView modelAndView = new ModelAndView("signup");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveRegister(@ModelAttribute("user") User user, BindingResult bindingResult,
			@RequestParam("dobproof") MultipartFile dobProof, @RequestParam("addressproof") MultipartFile addressProof,
			@RequestParam("aadharproof") MultipartFile aadharProof) throws Exception {
		String errorMsg = validateRegistration(user, dobProof, addressProof, aadharProof);
		ModelAndView modelAndView = null;
		if (errorMsg != null) {
			modelAndView = new ModelAndView("signup");
			modelAndView.addObject("user", new User());
			modelAndView.addObject("error", errorMsg);
		} else {
			if (!accountService.isUserNameExists(user.getUserName())) {
				registerService.saveRegister(user);
				modelAndView = new ModelAndView("login");
				modelAndView.addObject("user", new User());
				util.saveDocFile(util.getDobDirectory(user.getUserName()), dobProof);
				util.saveDocFile(util.getAddressDirectory(user.getUserName()), addressProof);
				util.saveDocFile(util.getAadharDirectory(user.getUserName()), aadharProof);
				modelAndView.addObject("success", "Registration Success! Please log in.");
			} else {
				modelAndView = new ModelAndView("signup");
				modelAndView.addObject("user", new User());
				modelAndView.addObject("error", "This Username is already registered.");
			}
		}
		return modelAndView;
	}

	@RequestMapping(value = "/getall", method = RequestMethod.GET)
	public ModelAndView getAllUsers() {

		return new ModelAndView("");
	}

	private String validateRegistration(User user, MultipartFile dobProof, MultipartFile addressProof,
			MultipartFile aadharProof) {
		String errorMsg = null;
		if (user == null) {
			errorMsg = "Please provide required details.";
		} else if (StringUtils.isEmpty(user.getFirstName())) {
			errorMsg = "Please provide First Name.";
		} else if (StringUtils.isEmpty(user.getLastName())) {
			errorMsg = "Please provide Last Name.";
		} else if (StringUtils.isEmpty(user.getTextDob())) {
			errorMsg = "Please provide DOB.";
		} else if (StringUtils.isEmpty(user.getAddLine1())) {
			errorMsg = "Please provide Address Line 1.";
		} else if (StringUtils.isEmpty(user.getCity())) {
			errorMsg = "Please provide City.";
		} else if (StringUtils.isEmpty(user.getState())) {
			errorMsg = "Please provide State.";
		} else if (StringUtils.isEmpty(user.getPin())) {
			errorMsg = "Please provide Pin Code.";
		} else if (StringUtils.isEmpty(user.getMobileNumber())) {
			errorMsg = "Please provide Mobile Number.";
		} else if (StringUtils.isEmpty(user.getEmailId())) {
			errorMsg = "Please provide Email.";
		} else if (StringUtils.isEmpty(user.getAadharId())) {
			errorMsg = "Please provide Aadhar.";
		} else if (StringUtils.isEmpty(user.getPan())) {
			errorMsg = "Please provide Pan.";
		} else if (StringUtils.isEmpty(user.getUserName())) {
			errorMsg = "Please provide Username.";
		} else if (StringUtils.isEmpty(user.getPassword())) {
			errorMsg = "Please provide Password.";
		} else if (StringUtils.isEmpty(user.getConfirmPassword())) {
			errorMsg = "Please provide Confirm Password.";
		} else if (!user.getPassword().equals(user.getConfirmPassword())) {
			errorMsg = "Password and Confirm Password does not match.";
		} else if (dobProof == null || dobProof.isEmpty() || addressProof == null || addressProof.isEmpty()
				|| aadharProof == null || aadharProof.isEmpty()) {
			errorMsg = "Please upload DOB Proof, Address Proof and Aadhar documents.";
		} else if (!util.isValidDocFile(dobProof.getOriginalFilename())
				|| !util.isValidDocFile(addressProof.getOriginalFilename())
				|| !util.isValidDocFile(aadharProof.getOriginalFilename())) {
			errorMsg = "Please upload DOB Proof, Address Proof and Aadhar documents in jpg, jpeg or pdf formats only.";
		} else {
			try {
				DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				Date date = format.parse(user.getTextDob());
				user.setDob(date);
			} catch (ParseException e) {
				errorMsg = "Password provide DOB in mm/dd/yyyy format";
			}
		}

		return errorMsg;
	}

}
