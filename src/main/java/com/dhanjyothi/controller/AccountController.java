package com.dhanjyothi.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.dhanjyothi.model.Account;
import com.dhanjyothi.model.User;
import com.dhanjyothi.service.AccountService;
import com.dhanjyothi.util.DhanJyothiUtil;

@Controller
public class AccountController {

	@Autowired
	AccountService accountService;
	
	@Autowired
	DhanJyothiUtil util;

	@GetMapping("/createsavingsaccount")
	public String loadAccountCreationPage(Model model, HttpServletRequest request) throws Exception {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		Account account = accountService.getAccountDetails(user.getUserId(), "S");
		if (account == null) {
			accountService.openSavingsAccount(user);
			account = accountService.getAccountDetails(user.getUserId(), "S");
		}
		request.getSession().setAttribute("account", account);
		model.addAttribute("account", account);
		model.addAttribute("taccount", new Account());
		model.addAttribute("user", user);
		List<Account> termAccounts = accountService.getTermAccountDetails(user.getUserId(), "T");
		request.getSession().setAttribute("taccounts", termAccounts);
		model.addAttribute("taccounts", request.getSession().getAttribute("taccounts"));
		return "accountsummary";

	}

	@GetMapping("/loadtermaccount")
	public String loadTermAccount(@ModelAttribute("account") Account account, Model model) {

		return null;
	}

	@PostMapping("/createtermaccount")
	public String createTermAccount(@ModelAttribute("taccount") Account account, Model model,
			HttpServletRequest request, BindingResult bindingResult) throws Exception {
		String errorMsg = null;
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		//MathContext mc = new MathContext(2);
		Account sbAccount = accountService.getAccountDetails(user.getUserId(), "S");
		if (account == null || account.getAccountBalance() == null
				|| account.getAccountBalance().compareTo(new BigDecimal("1000")) == -1
				|| account.getAccountBalance().compareTo(new BigDecimal("10000000")) == 1) {
			errorMsg = "Please provide term amount between 1000 to 10000000.";
		} else if (sbAccount == null || sbAccount.getAccountBalance().subtract(account.getAccountBalance())
				.compareTo(new BigDecimal("0")) == -1) {
			errorMsg = "Insufficient balance in Savings Account!";
		}
		if (errorMsg != null) {
			model.addAttribute("error", errorMsg);
		} else {
			accountService.openTermAccount(account, user);
			List<Account> termAccounts = accountService.getTermAccountDetails(user.getUserId(), "T");
			request.getSession().setAttribute("taccounts", termAccounts);
		}
		sbAccount = accountService.getAccountDetails(user.getUserId(), "S");
		model.addAttribute("account", sbAccount);
		model.addAttribute("taccount", new Account());
		model.addAttribute("user", user);
		model.addAttribute("taccounts", request.getSession().getAttribute("taccounts"));
		return "accountsummary";
	}

	@GetMapping("/viewtransactions")
	public String loadTransactionDetails(Model model, HttpServletRequest request) throws Exception {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return util.logout(request);
		}
		Account sbAccount = accountService.getAccountDetails(user.getUserId(), "S");
		if (sbAccount != null) {
			model.addAttribute("transactions", accountService.loadAllTransactions(sbAccount.getAccountId()));
			model.addAttribute("accountNumber", sbAccount.getAccountId());
		}
		return "transactions";
	}
}
