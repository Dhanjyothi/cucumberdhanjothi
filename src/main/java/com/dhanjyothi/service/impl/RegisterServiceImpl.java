package com.dhanjyothi.service.impl;

import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dhanjyothi.dao.RegisterDao;
import com.dhanjyothi.model.User;
import com.dhanjyothi.service.RegisterService;

@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	RegisterDao registerDao;

	public void saveRegister(User customer) {
		customer.setPassword(Base64.getEncoder().encodeToString(customer.getPassword().getBytes()));
		customer.setUserRole('C');
		customer.setUserStatus('N');
		registerDao.saveRegister(customer);
	}

	public List<User> getAllUsers() {
		return registerDao.getAllUsers();
	}

}
