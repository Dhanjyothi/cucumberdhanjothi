package com.dhanjyothi.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dhanjyothi.dao.AccountDao;
import com.dhanjyothi.model.Account;
import com.dhanjyothi.model.Beneficiaries;
import com.dhanjyothi.model.Transaction;
import com.dhanjyothi.model.User;
import com.dhanjyothi.service.AccountService;
import com.dhanjyothi.util.DhanJyothiUtil;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountDao accountDao;

	@Autowired
	DhanJyothiUtil util;

	BigDecimal[][] interestRates = { { new BigDecimal(5.75), new BigDecimal(6.00), new BigDecimal(6.25) },
			{ new BigDecimal(5.75), new BigDecimal(6.00), new BigDecimal(6.50) },
			{ new BigDecimal(5.75), new BigDecimal(6.25), new BigDecimal(6.75) },
			{ new BigDecimal(6.00), new BigDecimal(6.25), new BigDecimal(6.75) },
			{ new BigDecimal(6.00), new BigDecimal(6.50), new BigDecimal(7.00) } };

	public Account getAccountDetails(int userId, String accountType) throws Exception {
		return accountDao.getAccountDetails(userId, accountType);
	}

	public void openSavingsAccount(User user) throws Exception {
		Account account = new Account();
		account.setAccountBalance(new BigDecimal(10000));
		account.setAccountCreatedDate(new Date());
		account.setAccountHolderId(user.getUserId());
		account.setAccountStatus('A');
		account.setAccountType('S');
		account.setAccountUpdatedDate(new Date());
		account.setInterestRate(new BigDecimal(5));
		accountDao.openSavingsAccount(account, false);
	}

	public void openTermAccount(Account account, User user) throws Exception {
		Account sbAccount = getAccountDetails(user.getUserId(), "S");
		sbAccount.setAccountBalance(sbAccount.getAccountBalance().subtract(account.getAccountBalance()));
		updateSavingsAccount(sbAccount, user);

		account.setAccountCreatedDate(new Date());
		account.setAccountHolderId(user.getUserId());
		account.setAccountStatus('A');
		account.setAccountType('T');
		account.setAccountUpdatedDate(new Date());
		account.setInterestRate(new BigDecimal(
				util.getInterstRate(account.getDepositTenure(), account.getAccountBalance().longValue())));
		/*
		 * if (account.getAccountBalance().compareTo(new BigDecimal("100000")) == -1) {
		 * account.setInterestRate(interestRates[account.getDepositTenure() - 1][0]); }
		 * else if (account.getAccountBalance().compareTo(new BigDecimal("1000000")) ==
		 * -1) { account.setInterestRate(interestRates[account.getDepositTenure() -
		 * 1][1]); } else {
		 * account.setInterestRate(interestRates[account.getDepositTenure() - 1][2]); }
		 */
		accountDao.openTermAccount(account);

		Transaction transaction = new Transaction();
		transaction.setCreditAccount(account.getAccountId());
		transaction.setDebitAccount(sbAccount.getAccountId());
		transaction.setTrnAmount(account.getAccountBalance());
		transaction.setTrnDateTime(new Date());
		transaction.setTrnDesc("Self Transfer Term Account Creation");
		accountDao.updateTransactionDetails(transaction);
	}

	public List<Account> getTermAccountDetails(int userId, String accountType) throws Exception {
		return accountDao.getTermAccountDetails(userId, accountType);
	}

	public Map<Integer, String> getTenureDetails() {
		return null;
	}

	public boolean checkSavingsAccBalance(long termAmt) throws Exception {
		return true;
	}

	public void updateSavingsAccount(Account account, User cust) throws Exception {
		accountDao.openSavingsAccount(account, false);
	}

	public User getUserDetails(User user) throws Exception {
		return null;
	}

	public void saveBeneficiaries(Account account, Beneficiaries beneficiaries) throws Exception {
		accountDao.saveBeneficiaries(beneficiaries);
	}

	public boolean checkAccountExists(Beneficiaries beneficiaries) throws Exception {
		Account account = accountDao.checkAccountExists(beneficiaries.getBenAccountNum().intValue());
		return account == null ? false : true;
	}

	public List<Beneficiaries> getAllBeneficiaries(int userId) throws Exception {
		return accountDao.getAllBeneficiaries(userId);
	}

	public void updateFromAccount(Account account, long transAmt, Transaction transaction) throws Exception {
		account.setAccountBalance(account.getAccountBalance().subtract(transaction.getTrnAmount()));
		account.setAccountUpdatedDate(new Date());
		accountDao.openSavingsAccount(account, false);
		transaction.setDebitAccount(account.getAccountId());
	}

	public void updateToAccount(Transaction transaction) throws Exception {
		Account account = accountDao.getAccountDetails(transaction.getCreditAccount());
		account.setAccountBalance(account.getAccountBalance().add(transaction.getTrnAmount()));
		account.setAccountUpdatedDate(new Date());
		accountDao.openSavingsAccount(account, false);
		transaction.setTrnDateTime(new Date());
		accountDao.updateTransactionDetails(transaction);
	}

	public List<Transaction> loadAllTransactions(int accId) throws Exception {
		return accountDao.loadAllTransactions(accId);
	}

	public boolean isUserNameExists(String name) throws Exception {
		return accountDao.isUserNameExists(name) != null ? true : false;
	}

	public Account checkAccountExists(int accountId) throws Exception {
		return null;

	}

	public User getUserById(int userId) throws Exception {
		return accountDao.getUserById(userId);
	}

	@Override
	public void updateUser(User user) throws Exception {
		accountDao.updateUser(user);
		
	}
}
