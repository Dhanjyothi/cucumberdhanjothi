package com.dhanjyothi.service.impl;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dhanjyothi.dao.LoginDao;
import com.dhanjyothi.model.User;
import com.dhanjyothi.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	LoginDao loginDao;

	public int validateUser(User user) {
		if (user != null && user.getUserName() != null && user.getPassword() != null) {
			String encryptedPassword = Base64.getEncoder().encodeToString(user.getPassword().getBytes());
			user.setPassword(encryptedPassword);
			return loginDao.validateUser(user);
		}
		return 0;
	}

	@Override
	public User getUser(Integer userId) {
		return loginDao.getUser(userId);
	}

}
