package com.dhanjyothi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="transaction")
public class Transaction {

	@Id
	@Column(name="TRN_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer trnId;
	
	@Column(name="DEBIT_ACCT")
	private Integer debitAccount;
	
	@Column(name="CREDIT_ACCT")
	private Integer creditAccount;
	
	@Column(name="TRN_DESC")
	private String trnDesc;
	
	@Column(name="TRN_AMT")
	private BigDecimal trnAmount;
	
	@Column(name="TRN_DT_TIME")
	private Date trnDateTime;

	public Integer getTrnId() {
		return trnId;
	}

	public void setTrnId(Integer trnId) {
		this.trnId = trnId;
	}

	public Integer getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(Integer debitAccount) {
		this.debitAccount = debitAccount;
	}

	public Integer getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(Integer creditAccount) {
		this.creditAccount = creditAccount;
	}

	public String getTrnDesc() {
		return trnDesc;
	}

	public void setTrnDesc(String trnDesc) {
		this.trnDesc = trnDesc;
	}

	public BigDecimal getTrnAmount() {
		return trnAmount;
	}

	public void setTrnAmount(BigDecimal trnAmount) {
		this.trnAmount = trnAmount;
	}

	public Date getTrnDateTime() {
		return trnDateTime;
	}

	public void setTrnDateTime(Date trnDateTime) {
		this.trnDateTime = trnDateTime;
	}
	
}
