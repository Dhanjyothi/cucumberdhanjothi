package com.dhanjyothi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="kyc_document")
public class KYC {
	
	@Id
	@Column(name="KYC_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer kycId;
	
	@Column(name="USER_ID")
	private Integer userId;
	
	@Column(name="KYC_TYPE")
	private char kycType;
	
	@Column(name="DOCUMENT_DESC")
	private String documentDesc;

	public Integer getKycId() {
		return kycId;
	}

	public void setKycId(Integer kycId) {
		this.kycId = kycId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public char getKycType() {
		return kycType;
	}

	public void setKycType(char kycType) {
		this.kycType = kycType;
	}

	public String getDocumentDesc() {
		return documentDesc;
	}

	public void setDocumentDesc(String documentDesc) {
		this.documentDesc = documentDesc;
	}
	
}
