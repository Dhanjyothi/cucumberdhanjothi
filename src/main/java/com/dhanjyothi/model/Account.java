package com.dhanjyothi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@Column(name = "ACCT_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer accountId;
	
	@Column(name = "ACCT_HOLDER_ID")
	private Integer accountHolderId;
	
	@Column(name = "ACCT_TYPE")
	private char accountType;

	@Column(name = "INT_RATE")
	private BigDecimal interestRate;

	@Column(name = "ACC_BALANCE")
	private BigDecimal accountBalance;

	@Column(name = "DEPOSIT_TENURE")
	private Integer depositTenure;

	@Column(name = "MATURITY_AMT")
	private BigDecimal maturityAmount;

	@Column(name = "ACCOUNT_CREATED_DATE")
	private Date accountCreatedDate;

	@Column(name = "ACCOUNT_UPDATED_DATE")
	private Date accountUpdatedDate;

	@Column(name = "ACC_STATUS")
	private char accountStatus;

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public Integer getAccountHolderId() {
		return accountHolderId;
	}

	public void setAccountHolderId(Integer accountHolderId) {
		this.accountHolderId = accountHolderId;
	}

	public char getAccountType() {
		return accountType;
	}

	public void setAccountType(char accountType) {
		this.accountType = accountType;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Integer getDepositTenure() {
		return depositTenure;
	}

	public void setDepositTenure(Integer depositTenure) {
		this.depositTenure = depositTenure;
	}

	public BigDecimal getMaturityAmount() {
		return maturityAmount;
	}

	public void setMaturityAmount(BigDecimal maturityAmount) {
		this.maturityAmount = maturityAmount;
	}

	public Date getAccountCreatedDate() {
		return accountCreatedDate;
	}

	public void setAccountCreatedDate(Date accountCreatedDate) {
		this.accountCreatedDate = accountCreatedDate;
	}

	public Date getAccountUpdatedDate() {
		return accountUpdatedDate;
	}

	public void setAccountUpdatedDate(Date accountUpdatedDate) {
		this.accountUpdatedDate = accountUpdatedDate;
	}

	public char getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(char accountStatus) {
		this.accountStatus = accountStatus;
	}
}