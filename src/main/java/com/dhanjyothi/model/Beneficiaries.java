package com.dhanjyothi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="beneficiary")
public class Beneficiaries {
	
	@Id
	@Column(name="BEN_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer beneficiaryId;
	
	@Column(name="OWNER_ID")
	private Integer ownerId;
	
	@Column(name="BEN_TYPE")
	private char beneficiaryType;
	
	@Column(name="BEN_NICK_NAME")
	private String benNickName;
	
	@Column(name="BEN_NAME")
	private String benName;
	
	@Column(name="BEN_ACCT_NUM")
	private Long benAccountNum;
	
	@Column(name="BEN_BANK")
	private String benBank;
	
	@Column(name="BEN_BANK_IFSC")
	private String benIfsc;

	public Integer getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(Integer beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public char getBeneficiaryType() {
		return beneficiaryType;
	}

	public void setBeneficiaryType(char beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}

	public String getBenNickName() {
		return benNickName;
	}

	public void setBenNickName(String benNickName) {
		this.benNickName = benNickName;
	}

	public String getBenName() {
		return benName;
	}

	public void setBenName(String benName) {
		this.benName = benName;
	}

	public Long getBenAccountNum() {
		return benAccountNum;
	}

	public void setBenAccountNum(Long benAccountNum) {
		this.benAccountNum = benAccountNum;
	}

	public String getBenBank() {
		return benBank;
	}

	public void setBenBank(String benBank) {
		this.benBank = benBank;
	}

	public String getBenIfsc() {
		return benIfsc;
	}

	public void setBenIfsc(String benIfsc) {
		this.benIfsc = benIfsc;
	}
}
